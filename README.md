# Gumtree Email Alerts #

## A web app to implement email alert functionality for gumtree.com.au. ##

Alas, this feature has now been implemented by Gumtree.

'Tis better to have programmed and lost, than to never have programmed at all.