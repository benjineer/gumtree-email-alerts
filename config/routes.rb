WebAlerts::Application.routes.draw do

  devise_for :users

  resources :home, only: :index

  resources :alerts, only: [ :index, :new, :create, :destroy ]

  resources :locations, only: :show

  root 'home#index'
  
end
