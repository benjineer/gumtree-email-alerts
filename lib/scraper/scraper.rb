require_relative 'gumtree/alerts.rb'

module Scraper

	def self.scrape_gumtree(cutoff_time = DateTime.now)
		alerts = Alert.where("created_at < ?", cutoff_time).find_each(batch_size: 200) {
			|alert|

			Scraper::Gumtree::Alerts.scrape(alert)
		}
	end

	def self.email_all
		count = 0

		Alert.find_each(batch_size: 200) {
			|alert|

			if alert.alert_items.empty?
				print "\nAlert has no items\n"

			else
				begin
					AlertMailer.alert_email(alert).deliver_now
					count += 1
					print "\nEmail sent\n"

				rescue Exception => ex
					alert.update(last_run_error: "Email Error: #{ex.message}")
					print "\n#{alert.last_run_error}\n"
				end							
			end
		}

		return count
	end

	def self.scrape_and_email
		scrape_gumtree
		email_count = email_all

		print "\n#{email_count} email#{'s' if email_count > 1} sent\n"
	end

end
