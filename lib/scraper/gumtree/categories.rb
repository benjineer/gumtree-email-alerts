
require 'yaml'
require_relative 'crawler/categories.rb'

module Scraper::Gumtree

  class Categories

    def self.scrape(verbose = false, base_url = Alert::BASE_URL)

  		print "\nScraping categories\n"

  		crawler = Crawler::Categories.new
  		crawler.base_url('http://www.gumtree.com.au')
  		crawler.path('/')

      begin
  		  results = crawler.crawl

      rescue Exception => ex
        raise "Crawl Error: #{ex.message}"
      end

      print "\nResults:\n#{results}\n" if verbose

      save_yaml(results['categories'])
      save_factories(results['categories'])
    end

    def self.save_yaml(category_array)
      File.open(Rails.root.join('db/categories.yml'), 'w') {
        |file|
        file.write(category_array.to_yaml)
      }
    end

    def self.save_factories(category_array)
      File.open(Rails.root.join('spec/factories/categories.rb'), 'w') {
        |file|

        file.write(%{
  FactoryGirl.define do

    factory :category do
      name \"name string\"
      ref 10101
      url_path \"url-path\"
    end\n})

        counter = 1

        category_array.each {
          |parent|
          parent_name = parent['name'].gsub(/\W+/, '_').downcase

          file.write(%{
    factory :#{parent_name}, aliases: [ :cat#{counter} ], class: Category do
      name \"#{parent['name']}\"
      ref #{parent['ref']}
      url_path \"#{parent['url_path']}\"
    end\n})

          counter += 1

          parent['categories'].each {
            |child|
            child_name = child['name'].gsub(/\W+/, '_').downcase + "_#{parent_name}"

            file.write(%{
    factory :#{child_name}, aliases: [ :cat#{counter} ], class: Category do
      name \"#{child['name']}\"
      ref #{child['ref']}
      url_path \"#{child['url_path']}\"
      association :category, factory: :#{parent_name}
    end\n})

            counter += 1
          }
        }


        file.write("\nend\n")
    }
    end

    def str_to_snake(str)
      return str.gsub!(/\W+/, '_')
    end
  end

end
