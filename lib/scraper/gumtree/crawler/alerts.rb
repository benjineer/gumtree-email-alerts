require_relative 'wombat/property/locators/href.rb'
require_relative 'wombat/property/locators/src.rb'
require_relative 'wombat/property/locators/factory.rb'

module Scraper
  module Gumtree
    module Crawler

      class Alerts
        include Wombat::Crawler
        
        error_message css: ".page-title"

        alert_items "css=#srchrslt-adtable div[itemprop='itemOffered']", :iterator do

        	title css: "[itemprop='name']"
          date css: ".rs-ad-date"
          image_url "css=[itemprop='image']", :src

          url "css=[itemprop='url']", :href do |u|
          	u.start_with?('http') ? u : URI.join(Alert::BASE_URL, u).to_s
          end

          description "css=[itemprop='description']" do |d|
          	d.squeeze(' ').gsub(/[\n]/, '<br />')
          end

          price "css=[itemprop='price']" do |p|
          	p ? p.gsub(/[\$,]/, '') : nil
          end

          area "css=.rs-ad-location-area" do |a|
          	Alerts.parse_location(a)
          end

          town "css=.rs-ad-location-suburb" do |t|
          	Alerts.parse_location(t)
          end

          ref "css=[itemprop='url']", :href do |r|
            r.split('/').last.to_i
          end

        end

        def self.parse_location(raw)
        	raw == nil ? nil : raw.chomp(',')
        end
      end

    end
  end
end
