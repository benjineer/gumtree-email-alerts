
module Scraper::Gumtree::Crawler

	class Locations
	  include Wombat::Crawler

    '//*[@id="homepage-locations"]/*/a[contains(@class, "location-heading")]'

    # states
	  states 'xpath=//*[@id="homepage-locations"]/*/a[contains(@class, "location-heading")]', :iterator do
	  	name xpath: 'text()'
	  	loc_type 'state'

	  	ref xpath: '@id' do |r|
	  		r.gsub('loc-', '')
      end

      url_path xpath: '@href' do |u|
        u.split('/')[1][2..-1]
      end

	  	regions_page 'xpath=.', :follow do

		  	# regions
		  	regions 'xpath=//*[@id="refine-search"]/div[2]/div[2]/div/ul/li/ul/li/ul/li/a', :iterator do
		  		name xpath: 'text()'
		  		loc_type 'region'

		  		ref xpath: '@href' do |r|
			  		r.split('/').last.gsub('l', '')
          end

          url_path xpath: '@href' do |u|
            u.split('/')[1][2..-1]
          end

			  	areas_page 'xpath=.', :follow do

			  		# areas
			  		areas 'xpath=//*[@id="refine-search"]/div[2]/div[2]/div/ul/li/ul/li/ul/li/ul/li/a', :iterator do
			  			name xpath: 'text()'
			  			loc_type 'area'

			  			ref xpath: '@href' do |r|
					  		r.split('/').last.gsub('l', '')
              end

              url_path xpath: '@href' do |u|
                u.split('/')[1][2..-1]
              end

					  	towns_page 'xpath=.', :follow do

					  		# towns
					  		towns 'xpath=//*[@id="refine-search"]/div[2]/div[2]/div/ul/li/ul/li/ul/li/ul/li/ul/li/a', :iterator do
					  			name xpath: 'text()'
					  			loc_type 'town'

					  			ref xpath: '@href' do |r|
							  		r.split('/').last.gsub('l', '')
                  end

                  url_path xpath: '@href' do |u|
                    u.split('/')[1][2..-1]
                  end
					  		end
					  	end
			  		end
			  	end
		  	end
	  	end
    end
	end
end
