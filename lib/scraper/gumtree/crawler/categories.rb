
module Scraper::Gumtree::Crawler

	class Categories
	  include Wombat::Crawler

    categories 'xpath=//*[@id="categoryId"]/option', :iterator do
      name xpath: 'text()'
      ref xpath: '@value'

      url_path xpath: 'text()' do |u|
        u.gsub(/,/, ' ').gsub(/[,&'-]/, '').gsub(/\//, '-').gsub(/ +/, '-').downcase
      end

      categories 'xpath=following-sibling::*[1]/option', :iterator do
        name xpath: 'text()'
        ref xpath: '@value'

        url_path xpath: 'text()' do |u|
          u.gsub(/,/, ' ').gsub(/[&'-]/, '').gsub(/\//, '-').gsub(/ +/, '-').downcase
        end
      end

    end
	end

end
