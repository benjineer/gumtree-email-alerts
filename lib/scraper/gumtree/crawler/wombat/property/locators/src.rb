#coding: utf-8

module Wombat
  module Property
    module Locators
      class Src < Base
        def locate(context, page = nil)
          node = locate_nodes(context)
          node = node.first unless node.is_a?(String)

          value = node[:src] if node
          super { value }
        end
      end
    end
  end
end