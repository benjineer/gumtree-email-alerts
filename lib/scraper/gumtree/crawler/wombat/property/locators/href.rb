#coding: utf-8

module Wombat
  module Property
    module Locators
      class Href < Base
        def locate(context, page = nil)
          node = locate_nodes(context)
          node = node.first unless node.is_a?(String)

          value = node[:href] if node
          super { value }
        end
      end
    end
  end
end