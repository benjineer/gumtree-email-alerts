
require 'yaml'
require_relative 'crawler/locations.rb'

module Scraper::Gumtree
  class Locations

    def self.scrape(verbose = false, base_url = Alert::BASE_URL)

  		print "\nScraping locations\n"

  		crawler = Crawler::Locations.new
  		crawler.base_url('http://www.gumtree.com.au')
  		crawler.path('/')

      begin
  		  results = crawler.crawl

      rescue Exception => ex
        raise "Crawl Error: #{ex.message}"
      end

      print "\nResults:\n#{results}\n" if verbose

      results['states'].each {
        |state|
        state['regions'] = state['regions_page'].first.delete('regions')
        state.delete('regions_page')

        state['regions'].each {
          |region|
          region['areas'] = region['areas_page'].first.delete('areas')
          region.delete('areas_page')

          region['areas'].each {
            |area|
            area['towns'] = area['towns_page'].first.delete('towns')
            area.delete('towns_page')
          }
        }
      }

      save_yaml(results['states'])
      save_factories(results['states'])
    end

    def self.save_yaml(location_array)
      File.open(Rails.root.join('db/locations.yml'), 'w') {
        |file|
        file.write(location_array.to_yaml)
      }
    end

    def self.save_factories(location_array)
      File.open(Rails.root.join('spec/factories/locations.rb'), 'w') {
        |file|

        file.write(%{
  FactoryGirl.define do

    factory :location do
      name \"name string\"
      ref 10101
      loc_type \"state\"
      url_path \"url-path\"
    end\n})

        counter = 1

        location_array.each {
          |state|
          state_name = "loc#{counter}"

          file.write(%{
    factory :#{state_name}, class: Location do
      name \"#{state['name']}\"
      ref #{state['ref']}
      loc_type \"#{state['loc_type']}\"
      url_path \"#{state['url_path']}\"
    end\n})

          counter += 1

          state['regions'].each {
            |region|
            region_name = "loc#{counter}"

            file.write(%{
    factory :#{region_name}, class: Location do
      name \"#{region['name']}\"
      ref #{region['ref']}
      loc_type \"#{region['loc_type']}\"
      url_path \"#{region['url_path']}\"
      association :location, factory: :#{state_name}
    end\n})

            counter += 1

            region['areas'].each {
              |area|
              area_name = "loc#{counter}"

              file.write(%{
    factory :#{area_name}, class: Location do
      name \"#{area['name']}\"
      ref #{area['ref']}
      loc_type \"#{area['loc_type']}\"
      url_path \"#{area['url_path']}\"
      association :location, factory: :#{region_name}
    end\n})

              counter += 1

              area['towns'].each {
                |town|
                town_name = "loc#{counter}"

                file.write(%{
    factory :#{town_name}, class: Location do
      name \"#{town['name']}\"
      ref #{town['ref']}
      loc_type \"#{town['loc_type']}\"
      url_path \"#{town['url_path']}\"
      association :location, factory: :#{area_name}
    end\n})

                counter += 1
              }
            }
          }
        }

        file.write("\nend\n")
    }
    end

    def str_to_snake(str)
      return str.gsub!(/\W+/, '_')
    end

  end
end
