
require_relative 'crawler/alerts.rb'

module Scraper::Gumtree

  class Alerts

    def self.scrape(alert, verbose = false, base_url = Alert::BASE_URL)
  		url = alert.url

  		print "\nScraping #{url}\n"

  		crawler = Crawler::Alerts.new
  		crawler.base_url(base_url)
  		crawler.path(url)

      begin

        begin
    		  results = crawler.crawl

        rescue Exception => ex
          raise "Crawl Error: #{ex.message}"
        end

        print "\nResults:\n#{results}\n" if verbose
        
        raise "Page Error: #{results['error_message']}" if results['error_message']

        new_items = []

        results['alert_items'].each {
          |item|
          break if item['ref'] == alert.newest_ref
          new_items.push(item)
        }

        if new_items.count > 0
      		alert_items = alert.alert_items.create(new_items)
          alert.newest_ref = alert_items.first.ref

      		alert_items.each {
      			|item|
            unless item.errors.empty?
              raise "Item Save Error: #{item.errors.full_messages.first}"
            end
      		}
        end

      rescue Exception => ex
        alert.last_run_error = ex.message
        print "\n#{ex.message}\n" if verbose
      end
      
      alert.last_run = DateTime.now
      alert.save!
    end
  end

end
