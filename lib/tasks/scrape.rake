
require_relative '../scraper/gumtree/categories.rb'
require_relative '../scraper/gumtree/locations.rb'

namespace :scrape do

  desc "Scrapes categories from gumtree to /db/categories.yml and /spec/factories/categories.rb"
  task categories: :environment do
  	Scraper::Gumtree::Categories.scrape
  end

  desc "Scrapes locations from gumtree to /db/locations.yml"
  task locations: :environment do
  	start = DateTime.now

  	Scraper::Gumtree::Locations.scrape

  	minutes = (DateTime.now - start) * 24 * 60
  	print "\nFinished in #{minutes.to_i} minutes\n"
  end

end
