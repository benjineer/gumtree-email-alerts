
# read categories from scraped yaml
categories_fixture_path = Rails.root.join('db/categories.yml')
categories_string = File.read(categories_fixture_path)

YAML.load(categories_string).each {
	|item|

	children = item.delete('categories')
	category = Category.create(item)

	children.each {
		|child_item|		
		child = category.categories.build(child_item)
		child.save
	}
}

# read locations from scraped yaml
locations_fixture_path = Rails.root.join('db/locations.yml')
locations_string = File.read(locations_fixture_path)

YAML.load(locations_string).each {
	|state|
	regions = state.delete('regions')	
	state = Location.create(state)

	regions.each {
		|region|
		areas = region.delete('areas')
		region = state.locations.create(region)

		areas.each {
			|area|
			towns = area.delete('towns')
			area = region.locations.create(area)

			towns.each {
				|town|
				area.locations.create(town)
			}
		}
	}
}


# default user
user = User.new(
	email: '8enwilliams@gmail.com', 
	password: 'password12', 
	plain_text_email: false
)

user.skip_confirmation!
user.save!

