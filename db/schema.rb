# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150415101631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alert_items", force: :cascade do |t|
    t.string   "url"
    t.text     "title"
    t.text     "description"
    t.string   "image_url"
    t.string   "price"
    t.string   "area"
    t.string   "town"
    t.string   "date"
    t.integer  "alert_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ref"
  end

  add_index "alert_items", ["alert_id"], name: "index_alert_items_on_alert_id", using: :btree

  create_table "alerts", force: :cascade do |t|
    t.string   "name"
    t.string   "keyword"
    t.integer  "user_id"
    t.integer  "category_id"
    t.decimal  "min_price"
    t.decimal  "max_price"
    t.string   "offer_type"
    t.string   "price_type"
    t.integer  "newest_ref"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_run"
    t.string   "last_run_error"
    t.integer  "location_id"
    t.integer  "radius"
  end

  add_index "alerts", ["category_id"], name: "index_alerts_on_category_id", using: :btree
  add_index "alerts", ["user_id"], name: "index_alerts_on_user_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "ref"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.string   "url_path"
  end

  create_table "locations", force: :cascade do |t|
    t.string  "name"
    t.string  "ref"
    t.string  "loc_type"
    t.integer "location_id"
    t.string  "url_path"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.boolean  "plain_text_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
