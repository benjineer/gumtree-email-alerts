class AddLocations < ActiveRecord::Migration

  def up
  	create_table :locations do |t|
  		t.string :name
  		t.string :ref
      t.string :loc_type
  		t.belongs_to :location
  	end

  	add_column :alerts, :location_id, :integer, index: true
    add_column :alerts, :radius, :integer
  end

  def down
  	drop_table :locations
  	drop_column :alerts, :location_id
    drop_column :alerts, :radius
	end

end
