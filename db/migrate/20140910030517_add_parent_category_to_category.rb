class AddParentCategoryToCategory < ActiveRecord::Migration
  def up
  	add_column :categories, :category_id, :integer, index: true
  end

  def down
  	remove_column :categories, :category_id
  end
end
