class AddUrlPathToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :url_path, :string
  end
end
