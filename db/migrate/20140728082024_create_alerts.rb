class CreateAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.string :name
      t.string :keyword
      t.belongs_to :user, index: true
      t.references :category, index: true
      t.decimal :min_price
      t.decimal :max_price
      t.string :offer_type
      t.string :price_type
      t.integer :newest_ref

      t.timestamps
    end
  end
end
