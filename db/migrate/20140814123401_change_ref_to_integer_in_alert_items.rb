class ChangeRefToIntegerInAlertItems < ActiveRecord::Migration
  def change
  	remove_column :alert_items, :ref
  	add_column :alert_items, :ref, :integer
  end
end
