class RemoveUrlNameFromCategories < ActiveRecord::Migration
  def up
  	remove_column :categories, :url_name
  end

  def down
  	add_column :categories, :url, :string
  end
end
