class AddLastRunErrorToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :last_run_error, :string
  end
end
