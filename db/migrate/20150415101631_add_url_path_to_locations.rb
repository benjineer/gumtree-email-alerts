class AddUrlPathToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :url_path, :string
  end
end
