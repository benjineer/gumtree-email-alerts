class AddLastRunToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :last_run, :datetime
  end
end
