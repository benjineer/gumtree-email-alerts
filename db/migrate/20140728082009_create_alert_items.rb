class CreateAlertItems < ActiveRecord::Migration
  def change
    create_table :alert_items do |t|
      t.string :url
      t.string :ref
      t.text :title
      t.text :description
      t.string :image_url
      t.string :price
      t.string :area
      t.string :town
      t.string :date
      t.belongs_to :alert, index: true

      t.timestamps
    end
  end
end
