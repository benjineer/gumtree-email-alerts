module ApplicationHelper

	# <dt> & <dd>
	def def_tags(object, attribute, skip_blank = true, value = nil)

		if skip_blank && object[attribute].blank?
			html = ''

		else
			val = value == nil ? object[attribute] : value

			html = content_tag(:dt) do
				object.class.human_attribute_name(attribute)
			end +
			content_tag(:dd) do
				val
			end
		end

		return html
	end

end
