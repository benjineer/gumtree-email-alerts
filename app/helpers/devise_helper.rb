module DeviseHelper

  def devise_error_messages!
    return nil if !defined? resource
    return nil if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    return "<ul>#{messages}</ul>".html_safe
  end

end