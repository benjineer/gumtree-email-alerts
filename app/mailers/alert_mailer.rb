class AlertMailer < ActionMailer::Base
	helper :application

	def self.subject(alert)
		return "Gumtree Alert: #{alert.name}"
	end

  def alert_email(alert)
  	@alert = alert

		unless @alert.alert_items.blank?
	  	mail(subject: AlertMailer.subject(@alert), to: @alert.user.email)
			@alert.alert_items.destroy_all
		end
  end
end
