class AlertItem < ActiveRecord::Base

  belongs_to :alert, inverse_of: :alert_items
  
	validates :alert_id, :title, :url, :ref, presence: true

	before_save :check_existance

	# returns true if another item with the same ref and alert_id exists
	def exists?
		if self.new_record?
			exists = AlertItem.where("ref = ? AND alert_id = ?", self.ref, self.alert_id).exists?

		else
			exists = AlertItem.where("ref = ? AND alert_id = ? AND id <> ?", self.ref, self.alert_id, self.id).exists?
		end

		return exists
	end

	def check_existance
		errors.add(:ref, 'Item already exists') if exists?
	end

end
