class User < ActiveRecord::Base

	MAX_ALERTS = 3

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, 
         :lockable, :timeoutable

  has_many :alerts, inverse_of: :user, dependent: :delete_all

  before_update :check_alert_count

  def check_alert_count
  	errors.add(:alerts, "can't exceed #{MAX_ALERTS}") if self.alerts.count > MAX_ALERTS
  end

  def can_add_alert?
    return self.alerts.count < MAX_ALERTS
  end

end
