class Location < ActiveRecord::Base
  COUNT = 7827

  has_many :locations, inverse_of: :location
  belongs_to :location, inverse_of: :locations

  validates :name, :ref, :loc_type, :url_path, presence: true

  RADIUS_KMS = [ 0, 2, 5, 10, 20, 50, 250, 500 ]

  COUNTRY_TYPE = 'country'
  STATE_TYPE = 'state'
  REGION_TYPE = 'region'
  AREA_TYPE = 'area'
  TOWN_TYPE = 'town'

  def self.states
    return Location.where(loc_type: STATE_TYPE)
  end
end

