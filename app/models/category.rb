class Category < ActiveRecord::Base
	COUNT = 185

  belongs_to :category, inverse_of: :categories
  has_many :categories, inverse_of: :category

  validates :name, :url_path, :ref, presence: true
end
