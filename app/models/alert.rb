class Alert < ActiveRecord::Base

	OFFER_TYPES = { 'Offering' => 'offering', 'Wanted' => 'wanted' }
  
	PRICE_TYPES = { 
    'Fixed Price' => 'fixed', 
    'Negotiable' => 'negotiable', 
    'Free' => 'free',
    'Swap/Trade' => 'swap-trade'
  }

  BASE_URL = 'http://www.gumtree.com.au'

  belongs_to :user, inverse_of: :alerts
  belongs_to :category
  belongs_to :location
  has_many :alert_items, inverse_of: :alert, dependent: :delete_all

  before_save :clean_keyword!

  validates :user_id, presence: true
  validates :name, length: { minimum: 3, maximum: 300 }
  validates :keyword, length: { maximum: 300 }

  validates :radius, inclusion: { in: Location::RADIUS_KMS }
  
  validates :min_price, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validates :max_price, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  
  validates :offer_type, inclusion: { in: OFFER_TYPES.values, message: "%{value} is not a valid offer type" }, allow_blank: true
  validates :price_type, inclusion: { in: PRICE_TYPES.values, message: "%{value} is not a valid price type" }, allow_blank: true

  validate :category_valid
  validate :price_range_valid
  validate :last_run_date_valid

  def offer_type_str
  	return OFFER_TYPES.invert[self.offer_type]
  end

	def price_type_str
  	return PRICE_TYPES.invert[self.price_type]
  end

  def last_run_str
    if self.last_run
      self.last_run.in_time_zone.to_s(:aus_datetime).squeeze(' ').strip
    else
      ''
    end
  end

  def location_str
    location_name = location.blank? ? 'Anywhere' : location.name
    return "#{radius}km of #{location_name}"
  end

  def description
    str = keyword.blank? ? 'Anything' : "'#{keyword}'"
    str += ' in '
    str += category.blank? ? 'all categories' : "'#{category.name}'"
    str += ' from '
    str += min_price.blank? ? '$0' : '$%.2f' % min_price
    str += ' to '
    str += max_price.blank? ? 'infinity' : '$%.2f' % max_price
    str += " within #{location_str}"
    
    str += ", #{offer_type_str}" unless offer_type.blank?
    str += ", #{price_type_str}" unless price_type.blank?

    return str
  end

  def url
    save if changed?
    filters = filter_hash
    
    if category.blank? && keyword.blank?

      if filters.empty?
        if location.blank?
          url = '/'  # empty search
        else
          url = "/s-#{location.url_path}/#{location_url_str}"  # empty search with location
        end

      else
        url = "/s-ads"  # filters only
        url += "/#{location_url_str}" unless location.blank?  # filters and location
      end

    elsif category.blank?
      url = "/s-#{location.url_path}/#{url_keyword}/k0#{location_url_str}"  # keyword only

    elsif keyword.blank?
      url = "/s-#{category.url_path}#{location.url_path}/c#{category.ref}#{location_url_str}"  # category only

    else
      url = "/s-#{category.url_path}/#{location.url_path}/#{url_keyword}/k0c#{category.ref}#{location_url_str}"  # category and keyword
    end

    url += "?#{filters.to_query}" unless filters.empty?

    return url
  end

  private

  def category_valid
    unless self.category_id.blank?
      unless Category.exists?(self.category_id)
        errors.add(:category_id, "doesn't exist")
      end
    end
  end

  def price_range_valid
    unless self.min_price.blank? || self.max_price.blank?
      if self.max_price < self.min_price
        errors.add(:min_price, "is greater than the maximum price")
      end
    end
  end

  def last_run_date_valid
    if self.last_run      
      if self.last_run > DateTime.now
        errors.add(:last_run, "can't be in the future")
      end
    end

    if !self.new_record?
      previous_value = Alert.find(self.id).last_run

      if previous_value
        if !self.last_run
          errors.add(:last_run, "value can't be replaced with nil")

        elsif previous_value > self.last_run
          errors.add(:last_run, "can't be before the previous value")
        end
      end

    end
  end

  # removes non-alphanumeric/space and multiple spaces
  def clean_keyword!
    unless self.keyword.blank?
      self.keyword = self.keyword.squeeze(' ').strip.gsub(/[^\da-z ]/i, '')
    end
  end

  # returns the price range for url
  def price_range_str
    if min_price.blank? && max_price.blank?
      return nil
    else
      return "#{min_price}__#{max_price}"
    end
  end

  def location_url_str
    return location.blank? ? '' : "l#{location.ref}r#{radius}"
  end

  def filter_hash
    filters = {}
    price_str = price_range_str

    filters['price'] = price_str if price_str
    filters['ad'] = offer_type if offer_type
    filters['price-type'] = price_type if price_type

    return filters
  end

  # replaces spaces with '+'
  def url_keyword
    return keyword.gsub(/ /, '+')
  end

end
