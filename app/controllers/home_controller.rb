class HomeController < ApplicationController

	def index
		redirect_to(alerts_path) if current_user
	end
	
end
