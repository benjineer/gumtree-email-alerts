class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :authenticate

  protected

  # basic authentication if set in app config
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "test" && password == "password"
    end
  end

  # Returns errors as a HTML list
  def format_errors(model)
    if model.errors.empty?
      return nil
    end

    str = "<h5>#{model.class.model_name.human} not saved</h5>\n<ul>"

    model.errors.full_messages.each {
      |msg|        
      str += "<li>#{msg}</li>\n"
    }

    return (str + "</ul>").html_safe
  end
  
end
