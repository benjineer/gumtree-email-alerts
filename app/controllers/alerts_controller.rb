class AlertsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_alert, only: [:destroy]

  # GET /alerts
  def index
    @alerts = current_user.alerts
    @user = current_user
  end

  # GET /alerts/new
  def new
    @alert = Alert.new
  end

  # POST /alerts
  # POST /alerts.json
  def create
    @alert = current_user.alerts.build(alert_params)

    respond_to do |format|
      if @alert.save
        format.html { redirect_to alerts_url, notice: "Alert \"#{@alert.name}\" created" }
        format.json { render action: 'show', status: :created, location: @alert }
      else
        flash[:alert] = format_errors(@alert)
        format.html { render action: 'new' }
        format.json { render json: @alert.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alerts/1
  # DELETE /alerts/1.json
  def destroy
    if @alert
      flash[:notice] = "Alert \"#{@alert.name}\" deleted"
      @alert.destroy

    else
      flash[:alert] = "Alert not found!"
    end

    respond_to do |format|
      format.html { redirect_to alerts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_alert
      @alert = current_user.alerts.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alert_params
      params.require(:alert).permit(
        :name, 
        :keyword, 
        :category_id, 
        :min_price, 
        :max_price, 
        :offer_type, 
        :price_type, 
        :location_id, 
        :radius
      )
    end
end
