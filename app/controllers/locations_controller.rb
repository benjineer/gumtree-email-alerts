class LocationsController < ApplicationController

	def show
		respond_to do |format|

			format.json {
				id = params[:id]
				render json: Location.find(id).to_json(include: :locations)
			}

			format.html { render nothing: true}
		end
	end
end
