
$(document).ready(function () {

	/*********
	 * Index *
	 *********/

	var _deleteModalForm = $($('#modal-ok-button').parents('form'));
	var _initialDeleteUrl = _deleteModalForm.attr('action');

	// Show confirmation modal on delete click
	$('.delete-alert-button').click(function() {
		var modal = $('#confirm-delete-modal');
		var alertName = $(this).data('alert-name');
		var alertId = $(this).data('alert-id');		
		var url = _deleteModalForm.attr('action').replace(_alertIdPlaceholder, alertId);

		modal.find('.modal-body').html(
			'<h3>Delete "' + alertName + '"?<br />' +
			'<small>(Action can\'t be undone)</small></h3>'
		); // set text
		_deleteModalForm.attr('action', url); //set url

		modal.modal('show');
	});
	
	// Reset confirmation modal url on modal close
	$('#confirm-delete-modal').on('hide.bs.modal', function() {
		_deleteModalForm.attr('action', _initialDeleteUrl);
	});


	/*******
	 * New *
	 *******/

	// Initialise category selectors
	if ($('#parent-category').length) {
		new CategorySelectors('parent-category', 'child-category');
	}

	// Initialise location selectors
	if ($('#state-select').length) {
		new LocationSelectors(
			'state-select', 
			'region-select', 
			'area-select', 
			'town-select',
			'location-field'
		);
	}
});