
function CategorySelectors(parentId, childId) {
	this.parentSelect = $('#' + parentId);
	this.childSelect = $('#' + childId);

	for (var i = 0; i < _categories.length; ++i) {
		category = _categories[i];

		if (category.category_id == null) {
			var option = '';

			if (i == 0) {
				option = this.createOption(category, false, true);
			}
			else {
				option = this.createOption(category);
			}
			this.parentSelect.append(option);
		}
	}

	var parentChange = this.onParentChange;
	this.parentSelect.change(this.onParentChange.bind(this));
	this.parentSelect.change();
}

CategorySelectors.prototype.onParentChange = function () {
	var parentCategory = null;

	for (var i = 0; i < _categories.length; ++i) {

		if (_categories[i].id == this.parentSelect.val()) {
			parentCategory = _categories[i];
			break;
		}
	}

	this.childSelect.html('');
	this.childSelect.append(this.createOption(parentCategory, true));

	for (var i = 0; i < _categories.length; ++i) {

		if (_categories[i].category_id == parentCategory.id) {
			this.childSelect.append(this.createOption(_categories[i]));
		}
	}

	var childOptions = this.childSelect.find('option');
	if (childOptions.length == 1) {
		childOptions.attr('disabled', 'disabled');
		childOptions.text('All');
	}
};

CategorySelectors.prototype.createOption = function (category, isParent, isSelected) {
	var text = category.name;
	if (isParent) {
		text = 'All ' + text;
	}

	var selected = '';
	if (isSelected) {
		selected = ' selected="selected"';
	}

	return '<option value="' + category.id + '"' + selected + '>' + text + '</option>';
};
