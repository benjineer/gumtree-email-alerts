
function LocationSelectors(stateId, regionId, areaId, townId, valueId) {
	this.stateSelect = $('#' + stateId);
	this.regionSelect = $('#' + regionId);
	this.areaSelect = $('#' + areaId);
	this.townSelect = $('#' + townId);
	this.valueField = $('#' + valueId);

	this.stateSelect.change(this.onStateChange.bind(this));
	this.regionSelect.change(this.onRegionChange.bind(this));
	this.areaSelect.change(this.onAreaChange.bind(this));
	this.townSelect.change(this.onTownChange.bind(this));
}

LocationSelectors.prototype.onStateChange = function() {
	this.regionSelect.html('').attr('disabled', 'disabled');
	this.areaSelect.html('').addClass('hidden');
	this.townSelect.html('').addClass('hidden');

	if (this.stateSelect.val()) {
		var stateId = this.stateSelect.val();
		this.valueField.val(stateId);
		this.activateChild(stateId, this.regionSelect);
	}

	else {
		this.regionSelect.addClass('hidden');
		this.valueField.val('');
	}
};

LocationSelectors.prototype.onRegionChange = function() {
	this.areaSelect.html('').attr('disabled', 'disabled');
	this.townSelect.html('').addClass('hidden');

	if (this.regionSelect.val()) {
		var regionId = this.regionSelect.val();
		this.valueField.val(regionId);
		this.activateChild(regionId, this.areaSelect);
	}

	else {
		this.areaSelect.addClass('hidden');
		this.valueField.val(this.stateSelect.val());
	}
};

LocationSelectors.prototype.onAreaChange = function() {
	this.townSelect.html('').attr('disabled', 'disabled');

	if (this.areaSelect.val()) {
		var areaId = this.areaSelect.val();
		this.valueField.val(areaId);
		this.activateChild(areaId, this.townSelect);
	}

	else {
		this.townSelect.addClass('hidden');
		this.valueField.val(this.regionSelect.val());
	}
};

LocationSelectors.prototype.onTownChange = function() {
	if (this.townSelect.val()) {
		var townId = this.townSelect.val();
		this.valueField.val(townId);
	}

	else {
		this.valueField.val(this.areaSelect.val());
	}
};

LocationSelectors.prototype.activateChild = function(id, child) {

	$.ajax('/locations/' + id + '.json').done(function (parentLocation) {
		var options = "<option value=''>All</option>\n";

		parentLocation.locations.forEach(function (location) {
			options += "<option value='" + location.id + "'>" + location.name + "</option>\n";
		});

		child.html(options).removeClass('hidden').removeAttr('disabled');
	});
}
