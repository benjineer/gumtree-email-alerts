require 'rails_helper'

RSpec.describe "Alerts", :type => :request do
  describe "GET /alerts" do
    it "redirects to the login page" do
      get alerts_path
      expect(response.status).to be(302)
      expect(response['Location']).to match(/users\/sign_in/)
    end
  end
end
