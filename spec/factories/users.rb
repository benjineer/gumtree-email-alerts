# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
	sequence(:email) { |n| "#{n}@pzpzpzp.zxzxxz.co" }
	sequence(:password) { |n| "parsewurd#{n}" }

  factory :user do
    email
    password
    plain_text_email false
  	before(:create) { |user| user.skip_confirmation! }
  end
end
