# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence(:url) { |n| "http://www.gumtree.com.au/search/#{n}" }
  sequence(:ref) { |n| n }
  sequence(:title) { |n| "title_#{n}" }
  sequence(:description) { |n| "description+#{n}" }
  sequence(:image_url) { |n| "http://www.images.com/#{n}" }
  sequence(:price) { |n| 100 + n }
  sequence(:area) { |n| "area_#{n}" }
  sequence(:town) { |n| "town_#{n}" }
  sequence(:date) { |n| "#{n * 2} hours old" }

  factory :alert_item do
    url
    ref
    title
    description
    image_url
    price
    area
    town
    date
    alert
  end
end
