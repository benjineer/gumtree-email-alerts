
  FactoryGirl.define do

    factory :category do
      name "name string"
      ref 10101
      url_path "url-path"
    end

    factory :all_categories, aliases: [ :cat1 ], class: Category do
      name "All Categories"
      ref 0
      url_path "all-categories"
    end

    factory :antiques_art_collectables, aliases: [ :cat2 ], class: Category do
      name "Antiques, Art & Collectables"
      ref 18297
      url_path "antiques-art-collectables"
    end

    factory :antiques_antiques_art_collectables, aliases: [ :cat3 ], class: Category do
      name "Antiques"
      ref 20038
      url_path "antiques"
      association :category, factory: :antiques_art_collectables
    end

    factory :art_antiques_art_collectables, aliases: [ :cat4 ], class: Category do
      name "Art"
      ref 20039
      url_path "art"
      association :category, factory: :antiques_art_collectables
    end

    factory :collectables_antiques_art_collectables, aliases: [ :cat5 ], class: Category do
      name "Collectables"
      ref 20040
      url_path "collectables"
      association :category, factory: :antiques_art_collectables
    end

    factory :other_antiques_art_collectables_antiques_art_collectables, aliases: [ :cat6 ], class: Category do
      name "Other Antiques, Art & Collectables"
      ref 20041
      url_path "other-antiques-art-collectables"
      association :category, factory: :antiques_art_collectables
    end

    factory :automotive, aliases: [ :cat7 ], class: Category do
      name "Automotive"
      ref 9299
      url_path "automotive"
    end

    factory :automotive_services_automotive, aliases: [ :cat8 ], class: Category do
      name "Automotive Services"
      ref 18461
      url_path "automotive-services"
      association :category, factory: :automotive
    end

    factory :caravan_campervan_automotive, aliases: [ :cat9 ], class: Category do
      name "Caravan & Campervan"
      ref 18374
      url_path "caravan-campervan"
      association :category, factory: :automotive
    end

    factory :cars_vans_utes_automotive, aliases: [ :cat10 ], class: Category do
      name "Cars, Vans & Utes"
      ref 18320
      url_path "cars-vans-utes"
      association :category, factory: :automotive
    end

    factory :heavy_farming_agriculture_equipment_automotive, aliases: [ :cat11 ], class: Category do
      name "Heavy, Farming & Agriculture Equipment"
      ref 18460
      url_path "heavy-farming-agriculture-equipment"
      association :category, factory: :automotive
    end

    factory :mechanics_garages_automotive, aliases: [ :cat12 ], class: Category do
      name "Mechanics & Garages"
      ref 20009
      url_path "mechanics-garages"
      association :category, factory: :automotive
    end

    factory :motorcycles_scooters_automotive, aliases: [ :cat13 ], class: Category do
      name "Motorcycles & Scooters"
      ref 18322
      url_path "motorcycles-scooters"
      association :category, factory: :automotive
    end

    factory :other_automotive_automotive, aliases: [ :cat14 ], class: Category do
      name "Other Automotive"
      ref 18375
      url_path "other-automotive"
      association :category, factory: :automotive
    end

    factory :parts_accessories_automotive, aliases: [ :cat15 ], class: Category do
      name "Parts & Accessories"
      ref 18323
      url_path "parts-accessories"
      association :category, factory: :automotive
    end

    factory :trailers_automotive, aliases: [ :cat16 ], class: Category do
      name "Trailers"
      ref 20037
      url_path "trailers"
      association :category, factory: :automotive
    end

    factory :baby_children, aliases: [ :cat17 ], class: Category do
      name "Baby & Children"
      ref 18318
      url_path "baby-children"
    end

    factory :baby_carriers_baby_children, aliases: [ :cat18 ], class: Category do
      name "Baby Carriers"
      ref 18577
      url_path "baby-carriers"
      association :category, factory: :baby_children
    end

    factory :baby_clothing_baby_children, aliases: [ :cat19 ], class: Category do
      name "Baby Clothing"
      ref 18578
      url_path "baby-clothing"
      association :category, factory: :baby_children
    end

    factory :baths_baby_children, aliases: [ :cat20 ], class: Category do
      name "Baths"
      ref 18579
      url_path "baths"
      association :category, factory: :baby_children
    end

    factory :childcare_services_baby_children, aliases: [ :cat21 ], class: Category do
      name "Childcare Services"
      ref 18465
      url_path "childcare-services"
      association :category, factory: :baby_children
    end

    factory :cots_bedding_baby_children, aliases: [ :cat22 ], class: Category do
      name "Cots & Bedding"
      ref 18580
      url_path "cots-bedding"
      association :category, factory: :baby_children
    end

    factory :feeding_baby_children, aliases: [ :cat23 ], class: Category do
      name "Feeding"
      ref 18581
      url_path "feeding"
      association :category, factory: :baby_children
    end

    factory :toys_indoor_baby_children, aliases: [ :cat24 ], class: Category do
      name "Toys - Indoor"
      ref 18585
      url_path "toys-indoor"
      association :category, factory: :baby_children
    end

    factory :kids_clothing_baby_children, aliases: [ :cat25 ], class: Category do
      name "Kids Clothing"
      ref 20042
      url_path "kids-clothing"
      association :category, factory: :baby_children
    end

    factory :maternity_clothing_baby_children, aliases: [ :cat26 ], class: Category do
      name "Maternity Clothing"
      ref 18582
      url_path "maternity-clothing"
      association :category, factory: :baby_children
    end

    factory :other_baby_children_baby_children, aliases: [ :cat27 ], class: Category do
      name "Other Baby & Children"
      ref 18584
      url_path "other-baby-children"
      association :category, factory: :baby_children
    end

    factory :toys_outdoor_baby_children, aliases: [ :cat28 ], class: Category do
      name "Toys - Outdoor"
      ref 18586
      url_path "toys-outdoor"
      association :category, factory: :baby_children
    end

    factory :prams_strollers_baby_children, aliases: [ :cat29 ], class: Category do
      name "Prams & Strollers"
      ref 18583
      url_path "prams-strollers"
      association :category, factory: :baby_children
    end

    factory :safety_baby_children, aliases: [ :cat30 ], class: Category do
      name "Safety"
      ref 20096
      url_path "safety"
      association :category, factory: :baby_children
    end

    factory :boats_jet_skis, aliases: [ :cat31 ], class: Category do
      name "Boats & Jet Skis"
      ref 18420
      url_path "boats-jet-skis"
    end

    factory :boat_accessories_parts_boats_jet_skis, aliases: [ :cat32 ], class: Category do
      name "Boat Accessories & Parts"
      ref 20004
      url_path "boat-accessories-parts"
      association :category, factory: :boats_jet_skis
    end

    factory :jet_skis_boats_jet_skis, aliases: [ :cat33 ], class: Category do
      name "Jet Skis"
      ref 20024
      url_path "jet-skis"
      association :category, factory: :boats_jet_skis
    end

    factory :kayaks_paddle_boats_jet_skis, aliases: [ :cat34 ], class: Category do
      name "Kayaks & Paddle"
      ref 20025
      url_path "kayaks-paddle"
      association :category, factory: :boats_jet_skis
    end

    factory :motorboats_powerboats_boats_jet_skis, aliases: [ :cat35 ], class: Category do
      name "Motorboats & Powerboats"
      ref 20026
      url_path "motorboats-powerboats"
      association :category, factory: :boats_jet_skis
    end

    factory :other_boats_jet_skis_boats_jet_skis, aliases: [ :cat36 ], class: Category do
      name "Other Boats & Jet Skis"
      ref 20027
      url_path "other-boats-jet-skis"
      association :category, factory: :boats_jet_skis
    end

    factory :sail_boats_boats_jet_skis, aliases: [ :cat37 ], class: Category do
      name "Sail Boats"
      ref 20028
      url_path "sail-boats"
      association :category, factory: :boats_jet_skis
    end

    factory :tinnies_dinghies_boats_jet_skis, aliases: [ :cat38 ], class: Category do
      name "Tinnies & Dinghies"
      ref 20029
      url_path "tinnies-dinghies"
      association :category, factory: :boats_jet_skis
    end

    factory :books_music_games, aliases: [ :cat39 ], class: Category do
      name "Books, Music & Games"
      ref 18393
      url_path "books-music-games"
    end

    factory :books_books_music_games, aliases: [ :cat40 ], class: Category do
      name "Books"
      ref 20043
      url_path "books"
      association :category, factory: :books_music_games
    end

    factory :cds_dvds_books_music_games, aliases: [ :cat41 ], class: Category do
      name "CDs & DVDs"
      ref 18307
      url_path "cds-dvds"
      association :category, factory: :books_music_games
    end

    factory :board_games_books_music_games, aliases: [ :cat42 ], class: Category do
      name "Board Games"
      ref 18595
      url_path "board-games"
      association :category, factory: :books_music_games
    end

    factory :musical_instruments_books_music_games, aliases: [ :cat43 ], class: Category do
      name "Musical Instruments"
      ref 18409
      url_path "musical-instruments"
      association :category, factory: :books_music_games
    end

    factory :other_books_music_games_books_music_games, aliases: [ :cat44 ], class: Category do
      name "Other Books, Music & Games"
      ref 18596
      url_path "other-books-music-games"
      association :category, factory: :books_music_games
    end

    factory :clothing_jewellery, aliases: [ :cat45 ], class: Category do
      name "Clothing & Jewellery"
      ref 18308
      url_path "clothing-jewellery"
    end

    factory :accessories_clothing_jewellery, aliases: [ :cat46 ], class: Category do
      name "Accessories"
      ref 18575
      url_path "accessories"
      association :category, factory: :clothing_jewellery
    end

    factory :bags_clothing_jewellery, aliases: [ :cat47 ], class: Category do
      name "Bags"
      ref 18574
      url_path "bags"
      association :category, factory: :clothing_jewellery
    end

    factory :dress_making_alteration_services_clothing_jewellery, aliases: [ :cat48 ], class: Category do
      name "Dress Making & Alteration Services"
      ref 20013
      url_path "dress-making-alteration-services"
      association :category, factory: :clothing_jewellery
    end

    factory :jewellery_clothing_jewellery, aliases: [ :cat49 ], class: Category do
      name "Jewellery"
      ref 18601
      url_path "jewellery"
      association :category, factory: :clothing_jewellery
    end

    factory :men_s_clothing_clothing_jewellery, aliases: [ :cat50 ], class: Category do
      name "Men's Clothing"
      ref 18571
      url_path "mens-clothing"
      association :category, factory: :clothing_jewellery
    end

    factory :men_s_shoes_clothing_jewellery, aliases: [ :cat51 ], class: Category do
      name "Men's Shoes"
      ref 18573
      url_path "mens-shoes"
      association :category, factory: :clothing_jewellery
    end

    factory :women_s_clothing_clothing_jewellery, aliases: [ :cat52 ], class: Category do
      name "Women's Clothing"
      ref 18570
      url_path "womens-clothing"
      association :category, factory: :clothing_jewellery
    end

    factory :women_s_shoes_clothing_jewellery, aliases: [ :cat53 ], class: Category do
      name "Women's Shoes"
      ref 18572
      url_path "womens-shoes"
      association :category, factory: :clothing_jewellery
    end

    factory :community, aliases: [ :cat54 ], class: Category do
      name "Community"
      ref 9300
      url_path "community"
    end

    factory :activities_hobbies_community, aliases: [ :cat55 ], class: Category do
      name "Activities & Hobbies"
      ref 18324
      url_path "activities-hobbies"
      association :category, factory: :community
    end

    factory :classes_community, aliases: [ :cat56 ], class: Category do
      name "Classes"
      ref 18325
      url_path "classes"
      association :category, factory: :community
    end

    factory :dance_partners_community, aliases: [ :cat57 ], class: Category do
      name "Dance Partners"
      ref 20000
      url_path "dance-partners"
      association :category, factory: :community
    end

    factory :events_community, aliases: [ :cat58 ], class: Category do
      name "Events"
      ref 18483
      url_path "events"
      association :category, factory: :community
    end

    factory :garage_sale_community, aliases: [ :cat59 ], class: Category do
      name "Garage Sale"
      ref 18486
      url_path "garage-sale"
      association :category, factory: :community
    end

    factory :language_swap_community, aliases: [ :cat60 ], class: Category do
      name "Language Swap"
      ref 18330
      url_path "language-swap"
      association :category, factory: :community
    end

    factory :lost_found_community, aliases: [ :cat61 ], class: Category do
      name "Lost & Found"
      ref 18326
      url_path "lost-found"
      association :category, factory: :community
    end

    factory :missed_connections_community, aliases: [ :cat62 ], class: Category do
      name "Missed Connections"
      ref 18441
      url_path "missed-connections"
      association :category, factory: :community
    end

    factory :musicians_artists_community, aliases: [ :cat63 ], class: Category do
      name "Musicians & Artists"
      ref 18327
      url_path "musicians-artists"
      association :category, factory: :community
    end

    factory :other_community_community, aliases: [ :cat64 ], class: Category do
      name "Other Community"
      ref 18333
      url_path "other-community"
      association :category, factory: :community
    end

    factory :sports_partners_community, aliases: [ :cat65 ], class: Category do
      name "Sports Partners"
      ref 18331
      url_path "sports-partners"
      association :category, factory: :community
    end

    factory :rideshare_travel_partners_community, aliases: [ :cat66 ], class: Category do
      name "Rideshare & Travel Partners"
      ref 18332
      url_path "rideshare-travel-partners"
      association :category, factory: :community
    end

    factory :electronics_computer, aliases: [ :cat67 ], class: Category do
      name "Electronics & Computer"
      ref 20045
      url_path "electronics-computer"
    end

    factory :audio_electronics_computer, aliases: [ :cat68 ], class: Category do
      name "Audio"
      ref 21106
      url_path "audio"
      association :category, factory: :electronics_computer
    end

    factory :cameras_electronics_computer, aliases: [ :cat69 ], class: Category do
      name "Cameras"
      ref 18394
      url_path "cameras"
      association :category, factory: :electronics_computer
    end

    factory :computers_software_electronics_computer, aliases: [ :cat70 ], class: Category do
      name "Computers & Software"
      ref 18309
      url_path "computers-software"
      association :category, factory: :electronics_computer
    end

    factory :other_electronics_computers_electronics_computer, aliases: [ :cat71 ], class: Category do
      name "Other Electronics & Computers"
      ref 20046
      url_path "other-electronics-computers"
      association :category, factory: :electronics_computer
    end

    factory :phones_electronics_computer, aliases: [ :cat72 ], class: Category do
      name "Phones"
      ref 18313
      url_path "phones"
      association :category, factory: :electronics_computer
    end

    factory :tablets_ebooks_electronics_computer, aliases: [ :cat73 ], class: Category do
      name "Tablets & eBooks"
      ref 21121
      url_path "tablets-ebooks"
      association :category, factory: :electronics_computer
    end

    factory :tv_dvd_players_electronics_computer, aliases: [ :cat74 ], class: Category do
      name "TV & DVD players"
      ref 21127
      url_path "tv-dvd-players"
      association :category, factory: :electronics_computer
    end

    factory :video_games_consoles_electronics_computer, aliases: [ :cat75 ], class: Category do
      name "Video Games & Consoles"
      ref 18459
      url_path "video-games-consoles"
      association :category, factory: :electronics_computer
    end

    factory :tickets, aliases: [ :cat76 ], class: Category do
      name "Tickets"
      ref 18361
      url_path "tickets"
    end

    factory :bus_train_plane_tickets, aliases: [ :cat77 ], class: Category do
      name "Bus, Train & Plane"
      ref 20001
      url_path "bus-train-plane"
      association :category, factory: :tickets
    end

    factory :concerts_tickets, aliases: [ :cat78 ], class: Category do
      name "Concerts"
      ref 18484
      url_path "concerts"
      association :category, factory: :tickets
    end

    factory :other_tickets_tickets, aliases: [ :cat79 ], class: Category do
      name "Other Tickets"
      ref 18488
      url_path "other-tickets"
      association :category, factory: :tickets
    end

    factory :sport_tickets, aliases: [ :cat80 ], class: Category do
      name "Sport"
      ref 18485
      url_path "sport"
      association :category, factory: :tickets
    end

    factory :theatre_film_tickets, aliases: [ :cat81 ], class: Category do
      name "Theatre/Film"
      ref 20002
      url_path "theatre-film"
      association :category, factory: :tickets
    end

    factory :home_garden, aliases: [ :cat82 ], class: Category do
      name "Home & Garden"
      ref 18397
      url_path "home-garden"
    end

    factory :appliances_home_garden, aliases: [ :cat83 ], class: Category do
      name "Appliances"
      ref 20088
      url_path "appliances"
      association :category, factory: :home_garden
    end

    factory :building_materials_home_garden, aliases: [ :cat84 ], class: Category do
      name "Building Materials"
      ref 20103
      url_path "building-materials"
      association :category, factory: :home_garden
    end

    factory :builders_tradies_home_garden, aliases: [ :cat85 ], class: Category do
      name "Builders & Tradies"
      ref 18357
      url_path "builders-tradies"
      association :category, factory: :home_garden
    end

    factory :cleaning_services_home_garden, aliases: [ :cat86 ], class: Category do
      name "Cleaning Services"
      ref 18442
      url_path "cleaning-services"
      association :category, factory: :home_garden
    end

    factory :furniture_home_garden, aliases: [ :cat87 ], class: Category do
      name "Furniture"
      ref 20073
      url_path "furniture"
      association :category, factory: :home_garden
    end

    factory :garden_home_garden, aliases: [ :cat88 ], class: Category do
      name "Garden"
      ref 18398
      url_path "garden"
      association :category, factory: :home_garden
    end

    factory :home_decor_home_garden, aliases: [ :cat89 ], class: Category do
      name "Home Decor"
      ref 20082
      url_path "home-decor"
      association :category, factory: :home_garden
    end

    factory :kitchen_dining_home_garden, aliases: [ :cat90 ], class: Category do
      name "Kitchen & Dining"
      ref 21028
      url_path "kitchen-dining"
      association :category, factory: :home_garden
    end

    factory :lighting_home_garden, aliases: [ :cat91 ], class: Category do
      name "Lighting"
      ref 21027
      url_path "lighting"
      association :category, factory: :home_garden
    end

    factory :tools_diy_home_garden, aliases: [ :cat92 ], class: Category do
      name "Tools & DIY"
      ref 18430
      url_path "tools-diy"
      association :category, factory: :home_garden
    end

    factory :other_home_garden_home_garden, aliases: [ :cat93 ], class: Category do
      name "Other Home & Garden"
      ref 18403
      url_path "other-home-garden"
      association :category, factory: :home_garden
    end

    factory :jobs, aliases: [ :cat94 ], class: Category do
      name "Jobs"
      ref 9302
      url_path "jobs"
    end

    factory :construction_trades_jobs, aliases: [ :cat95 ], class: Category do
      name "Construction & Trades"
      ref 18346
      url_path "construction-trades"
      association :category, factory: :jobs
    end

    factory :sales_call_centres_jobs, aliases: [ :cat96 ], class: Category do
      name "Sales & Call Centres"
      ref 18491
      url_path "sales-call-centres"
      association :category, factory: :jobs
    end

    factory :farming_veterinary_jobs, aliases: [ :cat97 ], class: Category do
      name "Farming & Veterinary"
      ref 18384
      url_path "farming-veterinary"
      association :category, factory: :jobs
    end

    factory :finance_jobs, aliases: [ :cat98 ], class: Category do
      name "Finance"
      ref 18299
      url_path "finance"
      association :category, factory: :jobs
    end

    factory :gardening_landscaping_jobs, aliases: [ :cat99 ], class: Category do
      name "Gardening & Landscaping"
      ref 18385
      url_path "gardening-landscaping"
      association :category, factory: :jobs
    end

    factory :graphic_web_design_jobs, aliases: [ :cat100 ], class: Category do
      name "Graphic & Web Design"
      ref 18470
      url_path "graphic-web-design"
      association :category, factory: :jobs
    end

    factory :health_sports_beauty_jobs, aliases: [ :cat101 ], class: Category do
      name "Health,Sports & Beauty"
      ref 18464
      url_path "health-sports-beauty"
      association :category, factory: :jobs
    end

    factory :healthcare_nursing_jobs, aliases: [ :cat102 ], class: Category do
      name "Healthcare & Nursing"
      ref 18348
      url_path "healthcare-nursing"
      association :category, factory: :jobs
    end

    factory :hospitality_jobs, aliases: [ :cat103 ], class: Category do
      name "Hospitality"
      ref 18342
      url_path "hospitality"
      association :category, factory: :jobs
    end

    factory :housekeeping_jobs, aliases: [ :cat104 ], class: Category do
      name "Housekeeping"
      ref 18386
      url_path "housekeeping"
      association :category, factory: :jobs
    end

    factory :it_jobs, aliases: [ :cat105 ], class: Category do
      name "IT"
      ref 18344
      url_path "it"
      association :category, factory: :jobs
    end

    factory :real_estate_housing_jobs, aliases: [ :cat106 ], class: Category do
      name "Real Estate & Housing"
      ref 20036
      url_path "real-estate-housing"
      association :category, factory: :jobs
    end

    factory :legal_jobs, aliases: [ :cat107 ], class: Category do
      name "Legal"
      ref 20035
      url_path "legal"
      association :category, factory: :jobs
    end

    factory :moving_removals_jobs, aliases: [ :cat108 ], class: Category do
      name "Moving & Removals"
      ref 18463
      url_path "moving-removals"
      association :category, factory: :jobs
    end

    factory :nanny_babysitting_jobs, aliases: [ :cat109 ], class: Category do
      name "Nanny & Babysitting"
      ref 18350
      url_path "nanny-babysitting"
      association :category, factory: :jobs
    end

    factory :office_jobs, aliases: [ :cat110 ], class: Category do
      name "Office"
      ref 18351
      url_path "office"
      association :category, factory: :jobs
    end

    factory :other_jobs_jobs, aliases: [ :cat111 ], class: Category do
      name "Other Jobs"
      ref 18347
      url_path "other-jobs"
      association :category, factory: :jobs
    end

    factory :recruitment_hr_jobs, aliases: [ :cat112 ], class: Category do
      name "Recruitment & HR"
      ref 18387
      url_path "recruitment-hr"
      association :category, factory: :jobs
    end

    factory :resume_writing_services_jobs, aliases: [ :cat113 ], class: Category do
      name "Resume Writing Services"
      ref 18449
      url_path "resume-writing-services"
      association :category, factory: :jobs
    end

    factory :retail_jobs, aliases: [ :cat114 ], class: Category do
      name "Retail"
      ref 18353
      url_path "retail"
      association :category, factory: :jobs
    end

    factory :sales_marketing_jobs, aliases: [ :cat115 ], class: Category do
      name "Sales & Marketing"
      ref 18354
      url_path "sales-marketing"
      association :category, factory: :jobs
    end

    factory :sports_healthclub_jobs, aliases: [ :cat116 ], class: Category do
      name "Sports & Healthclub"
      ref 18388
      url_path "sports-healthclub"
      association :category, factory: :jobs
    end

    factory :teaching_childcare_jobs, aliases: [ :cat117 ], class: Category do
      name "Teaching & Childcare"
      ref 18355
      url_path "teaching-childcare"
      association :category, factory: :jobs
    end

    factory :training_development_jobs, aliases: [ :cat118 ], class: Category do
      name "Training & Development"
      ref 18428
      url_path "training-development"
      association :category, factory: :jobs
    end

    factory :transport_logistics_jobs, aliases: [ :cat119 ], class: Category do
      name "Transport & Logistics"
      ref 18490
      url_path "transport-logistics"
      association :category, factory: :jobs
    end

    factory :volunteer_jobs, aliases: [ :cat120 ], class: Category do
      name "Volunteer"
      ref 18489
      url_path "volunteer"
      association :category, factory: :jobs
    end

    factory :pets, aliases: [ :cat121 ], class: Category do
      name "Pets"
      ref 18433
      url_path "pets"
    end

    factory :birds_pets, aliases: [ :cat122 ], class: Category do
      name "Birds"
      ref 18456
      url_path "birds"
      association :category, factory: :pets
    end

    factory :cats_kittens_pets, aliases: [ :cat123 ], class: Category do
      name "Cats & Kittens"
      ref 18435
      url_path "cats-kittens"
      association :category, factory: :pets
    end

    factory :dogs_puppies_pets, aliases: [ :cat124 ], class: Category do
      name "Dogs & Puppies"
      ref 18434
      url_path "dogs-puppies"
      association :category, factory: :pets
    end

    factory :fish_pets, aliases: [ :cat125 ], class: Category do
      name "Fish"
      ref 20022
      url_path "fish"
      association :category, factory: :pets
    end

    factory :horses_ponies_pets, aliases: [ :cat126 ], class: Category do
      name "Horses & Ponies"
      ref 18632
      url_path "horses-ponies"
      association :category, factory: :pets
    end

    factory :livestock_pets, aliases: [ :cat127 ], class: Category do
      name "Livestock"
      ref 18457
      url_path "livestock"
      association :category, factory: :pets
    end

    factory :lost_found_pets, aliases: [ :cat128 ], class: Category do
      name "Lost & Found"
      ref 18437
      url_path "lost-found"
      association :category, factory: :pets
    end

    factory :other_pets_pets, aliases: [ :cat129 ], class: Category do
      name "Other Pets"
      ref 18436
      url_path "other-pets"
      association :category, factory: :pets
    end

    factory :pet_products_pets, aliases: [ :cat130 ], class: Category do
      name "Pet Products"
      ref 18438
      url_path "pet-products"
      association :category, factory: :pets
    end

    factory :pet_services_pets, aliases: [ :cat131 ], class: Category do
      name "Pet Services"
      ref 20018
      url_path "pet-services"
      association :category, factory: :pets
    end

    factory :rabbits_pets, aliases: [ :cat132 ], class: Category do
      name "Rabbits"
      ref 20023
      url_path "rabbits"
      association :category, factory: :pets
    end

    factory :reptiles_amphibians_pets, aliases: [ :cat133 ], class: Category do
      name "Reptiles & Amphibians"
      ref 18649
      url_path "reptiles-amphibians"
      association :category, factory: :pets
    end

    factory :real_estate, aliases: [ :cat134 ], class: Category do
      name "Real Estate"
      ref 9296
      url_path "real-estate"
    end

    factory :business_for_sale_real_estate, aliases: [ :cat135 ], class: Category do
      name "Business For Sale"
      ref 18468
      url_path "business-for-sale"
      association :category, factory: :real_estate
    end

    factory :land_for_sale_real_estate, aliases: [ :cat136 ], class: Category do
      name "Land For Sale"
      ref 20031
      url_path "land-for-sale"
      association :category, factory: :real_estate
    end

    factory :office_space_commercial_real_estate, aliases: [ :cat137 ], class: Category do
      name "Office Space & Commercial"
      ref 18365
      url_path "office-space-commercial"
      association :category, factory: :real_estate
    end

    factory :other_real_estate_real_estate, aliases: [ :cat138 ], class: Category do
      name "Other Real Estate"
      ref 18302
      url_path "other-real-estate"
      association :category, factory: :real_estate
    end

    factory :parking_storage_real_estate, aliases: [ :cat139 ], class: Category do
      name "Parking & Storage"
      ref 18366
      url_path "parking-storage"
      association :category, factory: :real_estate
    end

    factory :property_for_sale_real_estate, aliases: [ :cat140 ], class: Category do
      name "Property For Sale"
      ref 18367
      url_path "property-for-sale"
      association :category, factory: :real_estate
    end

    factory :real_estate_services_real_estate, aliases: [ :cat141 ], class: Category do
      name "Real Estate Services"
      ref 18467
      url_path "real-estate-services"
      association :category, factory: :real_estate
    end

    factory :property_for_rent_real_estate, aliases: [ :cat142 ], class: Category do
      name "Property for Rent"
      ref 18364
      url_path "property-for-rent"
      association :category, factory: :real_estate
    end

    factory :roomshare_real_estate, aliases: [ :cat143 ], class: Category do
      name "Roomshare"
      ref 18511
      url_path "roomshare"
      association :category, factory: :real_estate
    end

    factory :flatshare_houseshare_real_estate, aliases: [ :cat144 ], class: Category do
      name "Flatshare & Houseshare"
      ref 18294
      url_path "flatshare-houseshare"
      association :category, factory: :real_estate
    end

    factory :short_term_real_estate, aliases: [ :cat145 ], class: Category do
      name "Short Term"
      ref 18295
      url_path "short-term"
      association :category, factory: :real_estate
    end

    factory :services_for_hire, aliases: [ :cat146 ], class: Category do
      name "Services For Hire"
      ref 9303
      url_path "services-for-hire"
    end

    factory :appliance_phone_repair_services_for_hire, aliases: [ :cat147 ], class: Category do
      name "Appliance & Phone Repair"
      ref 20010
      url_path "appliance-phone-repair"
      association :category, factory: :services_for_hire
    end

    factory :automotive_services_for_hire, aliases: [ :cat148 ], class: Category do
      name "Automotive"
      ref 18461
      url_path "automotive"
      association :category, factory: :services_for_hire
    end

    factory :building_trades_services_for_hire, aliases: [ :cat149 ], class: Category do
      name "Building & Trades"
      ref 18357
      url_path "building-trades"
      association :category, factory: :services_for_hire
    end

    factory :childcare_nanny_services_for_hire, aliases: [ :cat150 ], class: Category do
      name "Childcare & Nanny"
      ref 18465
      url_path "childcare-nanny"
      association :category, factory: :services_for_hire
    end

    factory :cleaning_services_for_hire, aliases: [ :cat151 ], class: Category do
      name "Cleaning"
      ref 18442
      url_path "cleaning"
      association :category, factory: :services_for_hire
    end

    factory :computer_telecom_freelance_services_for_hire, aliases: [ :cat152 ], class: Category do
      name "Computer, Telecom & Freelance"
      ref 18358
      url_path "computer-telecom-freelance"
      association :category, factory: :services_for_hire
    end

    factory :courses_training_services_for_hire, aliases: [ :cat153 ], class: Category do
      name "Courses & Training"
      ref 18446
      url_path "courses-training"
      association :category, factory: :services_for_hire
    end

    factory :dress_making_alterations_services_for_hire, aliases: [ :cat154 ], class: Category do
      name "Dress Making & Alterations"
      ref 20013
      url_path "dress-making-alterations"
      association :category, factory: :services_for_hire
    end

    factory :entertainment_services_for_hire, aliases: [ :cat155 ], class: Category do
      name "Entertainment"
      ref 18443
      url_path "entertainment"
      association :category, factory: :services_for_hire
    end

    factory :graphic_web_design_services_for_hire, aliases: [ :cat156 ], class: Category do
      name "Graphic & Web Design"
      ref 18469
      url_path "graphic-web-design"
      association :category, factory: :services_for_hire
    end

    factory :health_beauty_services_for_hire, aliases: [ :cat157 ], class: Category do
      name "Health & Beauty"
      ref 18359
      url_path "health-beauty"
      association :category, factory: :services_for_hire
    end

    factory :landscaping_gardening_services_for_hire, aliases: [ :cat158 ], class: Category do
      name "Landscaping & Gardening"
      ref 18445
      url_path "landscaping-gardening"
      association :category, factory: :services_for_hire
    end

    factory :language_tutoring_services_for_hire, aliases: [ :cat159 ], class: Category do
      name "Language & Tutoring"
      ref 18466
      url_path "language-tutoring"
      association :category, factory: :services_for_hire
    end

    factory :mechanics_garages_services_for_hire, aliases: [ :cat160 ], class: Category do
      name "Mechanics & Garages"
      ref 20009
      url_path "mechanics-garages"
      association :category, factory: :services_for_hire
    end

    factory :musicians_artists_services_for_hire, aliases: [ :cat161 ], class: Category do
      name "Musicians & Artists"
      ref 18472
      url_path "musicians-artists"
      association :category, factory: :services_for_hire
    end

    factory :other_business_services_services_for_hire, aliases: [ :cat162 ], class: Category do
      name "Other Business Services"
      ref 18360
      url_path "other-business-services"
      association :category, factory: :services_for_hire
    end

    factory :party_catering_services_for_hire, aliases: [ :cat163 ], class: Category do
      name "Party & Catering"
      ref 18447
      url_path "party-catering"
      association :category, factory: :services_for_hire
    end

    factory :personal_training_services_for_hire, aliases: [ :cat164 ], class: Category do
      name "Personal Training"
      ref 18444
      url_path "personal-training"
      association :category, factory: :services_for_hire
    end

    factory :pet_services_services_for_hire, aliases: [ :cat165 ], class: Category do
      name "Pet Services"
      ref 20018
      url_path "pet-services"
      association :category, factory: :services_for_hire
    end

    factory :photography_video_services_for_hire, aliases: [ :cat166 ], class: Category do
      name "Photography & Video"
      ref 18448
      url_path "photography-video"
      association :category, factory: :services_for_hire
    end

    factory :real_estate_services_for_hire, aliases: [ :cat167 ], class: Category do
      name "Real Estate"
      ref 18467
      url_path "real-estate"
      association :category, factory: :services_for_hire
    end

    factory :removals_storage_services_for_hire, aliases: [ :cat168 ], class: Category do
      name "Removals & Storage"
      ref 18643
      url_path "removals-storage"
      association :category, factory: :services_for_hire
    end

    factory :resume_services_for_hire, aliases: [ :cat169 ], class: Category do
      name "Resume"
      ref 18449
      url_path "resume"
      association :category, factory: :services_for_hire
    end

    factory :tax_insurance_financial_services_for_hire, aliases: [ :cat170 ], class: Category do
      name "Tax, Insurance & Financial"
      ref 18451
      url_path "tax-insurance-financial"
      association :category, factory: :services_for_hire
    end

    factory :taxi_chauffeur_airport_transfer_services_for_hire, aliases: [ :cat171 ], class: Category do
      name "Taxi, Chauffeur & Airport Transfer"
      ref 18453
      url_path "taxi-chauffeur-airport-transfer"
      association :category, factory: :services_for_hire
    end

    factory :travel_agent_services_for_hire, aliases: [ :cat172 ], class: Category do
      name "Travel Agent"
      ref 18452
      url_path "travel-agent"
      association :category, factory: :services_for_hire
    end

    factory :wedding_venues_services_for_hire, aliases: [ :cat173 ], class: Category do
      name "Wedding & Venues"
      ref 18455
      url_path "wedding-venues"
      association :category, factory: :services_for_hire
    end

    factory :sport_fitness, aliases: [ :cat174 ], class: Category do
      name "Sport & Fitness"
      ref 18314
      url_path "sport-fitness"
    end

    factory :bicycles_sport_fitness, aliases: [ :cat175 ], class: Category do
      name "Bicycles"
      ref 18560
      url_path "bicycles"
      association :category, factory: :sport_fitness
    end

    factory :boxing_martial_arts_sport_fitness, aliases: [ :cat176 ], class: Category do
      name "Boxing & Martial Arts"
      ref 18561
      url_path "boxing-martial-arts"
      association :category, factory: :sport_fitness
    end

    factory :camping_hiking_sport_fitness, aliases: [ :cat177 ], class: Category do
      name "Camping & Hiking"
      ref 18562
      url_path "camping-hiking"
      association :category, factory: :sport_fitness
    end

    factory :fishing_sport_fitness, aliases: [ :cat178 ], class: Category do
      name "Fishing"
      ref 18563
      url_path "fishing"
      association :category, factory: :sport_fitness
    end

    factory :golf_sport_fitness, aliases: [ :cat179 ], class: Category do
      name "Golf"
      ref 18564
      url_path "golf"
      association :category, factory: :sport_fitness
    end

    factory :gym_fitness_sport_fitness, aliases: [ :cat180 ], class: Category do
      name "Gym & Fitness"
      ref 18565
      url_path "gym-fitness"
      association :category, factory: :sport_fitness
    end

    factory :sports_bags_sport_fitness, aliases: [ :cat181 ], class: Category do
      name "Sports Bags"
      ref 18566
      url_path "sports-bags"
      association :category, factory: :sport_fitness
    end

    factory :other_sports_fitness_sport_fitness, aliases: [ :cat182 ], class: Category do
      name "Other Sports & Fitness"
      ref 18569
      url_path "other-sports-fitness"
      association :category, factory: :sport_fitness
    end

    factory :personal_training_services_sport_fitness, aliases: [ :cat183 ], class: Category do
      name "Personal Training Services"
      ref 18444
      url_path "personal-training-services"
      association :category, factory: :sport_fitness
    end

    factory :racquet_sports_sport_fitness, aliases: [ :cat184 ], class: Category do
      name "Racquet Sports"
      ref 18559
      url_path "racquet-sports"
      association :category, factory: :sport_fitness
    end

    factory :skateboards_rollerblades_sport_fitness, aliases: [ :cat185 ], class: Category do
      name "Skateboards & Rollerblades"
      ref 18567
      url_path "skateboards-rollerblades"
      association :category, factory: :sport_fitness
    end

    factory :snow_sports_sport_fitness, aliases: [ :cat186 ], class: Category do
      name "Snow Sports"
      ref 20095
      url_path "snow-sports"
      association :category, factory: :sport_fitness
    end

    factory :surfing_sport_fitness, aliases: [ :cat187 ], class: Category do
      name "Surfing"
      ref 18568
      url_path "surfing"
      association :category, factory: :sport_fitness
    end

    factory :miscellaneous_goods, aliases: [ :cat188 ], class: Category do
      name "Miscellaneous Goods"
      ref 18319
      url_path "miscellaneous-goods"
    end

end
