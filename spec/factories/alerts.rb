
FactoryGirl.define do

  factory :alert do
    name 'default'
    keyword nil
    user
    category nil
    min_price nil
    max_price nil
    offer_type nil
    price_type nil
    newest_ref nil
    last_run nil
    last_run_error nil
    radius Location::RADIUS_KMS.first
  end

  factory :full_alert, class: Alert do
    name 'Wig Search'
    keyword 'wigs'
    user
    category
    location
    min_price 2
    max_price 100
    offer_type Alert::OFFER_TYPES['Offering']
    price_type Alert::PRICE_TYPES['Fixed Price']
    newest_ref 1234
    last_run DateTime.now
    last_run_error nil
    radius Location::RADIUS_KMS.last
    
    transient do
      alert_items_count 5
    end

    after(:create) do |alert, evaluator|
      create_list(:alert_item, evaluator.alert_items_count, alert: alert)
    end
  end

end
