
FactoryGirl.define do

  factory :test_alert do
    name 'default'
    url nil
    keyword nil
    user
    category nil
    min_price nil
    max_price nil
    offer_type nil
    price_type nil
    newest_ref nil
    last_run nil
    last_run_error nil
    radius Location::RADIUS_KMS.first
  end

end