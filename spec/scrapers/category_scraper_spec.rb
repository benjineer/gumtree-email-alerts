require 'rails_helper'
require_relative '../../lib/scraper/gumtree/categories.rb'

RSpec.describe Scraper::Gumtree::Categories, type: :scraper do

	it 'gets all categories', slow: true do
		Scraper::Gumtree::Categories.scrape

		count = 0

		categories_fixture_path = Rails.root.join('db/categories.yml')
		categories_string = File.read(categories_fixture_path)

		YAML.load(categories_string).each {
			|item|
			count += 1

			count += item['categories'].count
		}

		expect(count).to eq(Category::COUNT)
	end

end
