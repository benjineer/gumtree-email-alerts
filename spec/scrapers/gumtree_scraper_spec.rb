require 'rails_helper'
require_relative '../../lib/scraper/gumtree/alerts.rb'

HTML_404 = '404_error.html'
HTML_500 = '500_error.html'

HTML_0_ITEMS = '0_items.html'
HTML_4_ITEMS = '4_items.html'
HTML_25_ITEMS = '25_items.html'

HTML_SEARCH_A = 'search_a.html'
HTML_SEARCH_B = 'search_b.html'

NEWEST_REF = {
	HTML_4_ITEMS: 1052928422,
	HTML_25_ITEMS: 1053344878,
	HTML_SEARCH_A: 1054255597,
	HTML_SEARCH_B: 1054256170
}


RSpec.describe Scraper::Gumtree::Alerts, type: :scraper do

	it 'handles 404 error page' do
		result = scrape_file(HTML_404)

		expect(result.alert.newest_ref.blank?).to eq(true)
		expect(result.alert.alert_items.blank?).to eq(true)
		expect(result.last_run_ok).to eq(true)
		expect(result.alert.last_run_error).to match(/404/)
	end

	it 'handles 500 error page' do
		result = scrape_file(HTML_500)

		expect(result.alert.newest_ref.blank?).to eq(true)
		expect(result.alert.alert_items.blank?).to eq(true)
		expect(result.last_run_ok).to eq(true)
		expect(result.alert.last_run_error).to match(/500/)
	end

	it 'handles no results from search' do
		result = scrape_file(HTML_0_ITEMS)

		expect(result.alert.newest_ref.blank?).to eq(true)
		expect(result.alert.alert_items.blank?).to eq(true)
		expect(result.last_run_ok).to eq(true)
		expect(result.alert.last_run_error.blank?).to eq(true)
	end

	it 'only gets main content' do
		result_25 = scrape_file(HTML_25_ITEMS)
		result_4 = scrape_file(HTML_4_ITEMS)

		expect(result_4.alert.newest_ref).to eq(NEWEST_REF[:HTML_4_ITEMS])
		expect(result_4.alert.alert_items.count).to eq(4)
		expect(result_4.last_run_ok).to eq(true)
		expect(result_4.alert.last_run_error.blank?).to eq(true)

		expect(result_25.alert.newest_ref).to eq(NEWEST_REF[:HTML_25_ITEMS])
		expect(result_25.alert.alert_items.count).to eq(25)
		expect(result_25.last_run_ok).to eq(true)
		expect(result_25.alert.last_run_error.blank?).to eq(true)
	end

	it 'only gets all ads since the last scrape' do
		result_a = scrape_file(HTML_SEARCH_A)
		expect(result_a.alert.newest_ref).to eq(NEWEST_REF[:HTML_SEARCH_A])
		expect(result_a.alert.alert_items.count).to eq(25)
		expect(result_a.last_run_ok).to eq(true)
		expect(result_a.alert.last_run_error.blank?).to eq(true)

		result_a.alert.alert_items.destroy_all
		expect(result_a.alert.alert_items.count).to eq(0)

		result_b = scrape_file(HTML_SEARCH_B, result_a.alert)
		expect(result_b.alert.newest_ref).to eq(NEWEST_REF[:HTML_SEARCH_B])
		expect(result_b.alert.alert_items.count).to eq(2)
		expect(result_b.last_run_ok).to eq(true)
		expect(result_b.alert.last_run_error.blank?).to eq(true)
	end

end
