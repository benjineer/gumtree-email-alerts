require 'rails_helper'
require_relative '../../lib/scraper/gumtree/locations.rb'

RSpec.describe Scraper::Gumtree::Locations, type: :scraper do

	it 'gets all locations', slow: true do
		Scraper::Gumtree::Locations.scrape

		count = 0

		locations_fixture_path = Rails.root.join('db/locations.yml')
		locations_string = File.read(locations_fixture_path)

		YAML.load(locations_string).each {
			|state|
			count += 1
			regions = state['regions']

			regions.each {
				|region|
				count += 1
				areas = region['areas']

				areas.each {
					|area|
					count += 1
					towns = area['towns']

					towns.each {
						|town|
						count += 1
					}
				}
			}
		}
		expect(count).to be >= (Location::COUNT)
	end

end
