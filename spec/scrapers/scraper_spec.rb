require 'rails_helper'
require_relative '../../lib/scraper/scraper.rb'

RSpec.describe Scraper, type: :scraper do

	TEST_ALERT_LIMIT = 20
	
	before(:each) do
    Alert.destroy_all
  end

	it "gets results for #{TEST_ALERT_LIMIT} random alerts", slow: true do
		alerts = all_possible_alerts(TEST_ALERT_LIMIT)

		scrape_start = DateTime.now
		Scraper.scrape_gumtree
		scrape_end = DateTime.now

		alerts.each {
			|alert|

			alert.reload

			expect(alert.last_run).to be > scrape_start
			expect(alert.last_run).to be < scrape_end
			expect(alert.last_run_error).to be_nil
		}

		skip 'keeps memory usage below 3MB'
	end

	it 'takes less than 2 seconds to scrape an alert' do
		alert = create(:full_alert)
		
		scrape_start = DateTime.now
		Scraper.scrape_gumtree
		scrape_end = DateTime.now

		expect((scrape_start - scrape_end).seconds).to be < 2.seconds
	end

	it 'ignores alerts created after cutoff time', slow: true do
		early_alert = create(:alert)
		sleep(0.1.second)

		cutoff_time = DateTime.now
		sleep(0.1.second)

		late_alert = create(:alert)

		Scraper.scrape_gumtree(cutoff_time)
		early_alert.reload
		late_alert.reload

		expect(early_alert.last_run).not_to be_nil
		expect(late_alert.last_run).to be_nil

		Scraper.scrape_gumtree(DateTime.now)
		sleep(0.1.second)
		late_alert.reload

		expect(late_alert.last_run).not_to be_nil
	end

	it "only sends an email if alert has items" do
		empty_alert = create(:alert)
		alert_with_items = create(:full_alert)

		expect(Scraper.email_all).to eq(1)	
	end

end
