require 'rails_helper'

RSpec.describe "alert_mailer", type: :mailer do

  it "sends an email for an alert" do
    alert = create(:full_alert)
    subject = AlertMailer.subject(alert)

    expect { AlertMailer.alert_email(alert).deliver_now }.to change { ActionMailer::Base.deliveries.count }.by 1
    expect(ActionMailer::Base.deliveries.last.subject).to eq(subject)
  end

  it "takes less than 1 second to send an email" do
    mail_start = DateTime.now
    AlertMailer.alert_email(create(:full_alert)).deliver_now
    mail_end = DateTime.now

    expect((mail_start - mail_end).seconds).to be < 1.seconds
  end

  it "doesn't send an email for an alert with no items" do
    alert = create(:alert)
    
    expect(alert.alert_items.count).to eq(0)
    expect { AlertMailer.alert_email(alert).deliver_now }.not_to change { ActionMailer::Base.deliveries.count }
  end

  it "deletes all alert items" do
    alert = create(:full_alert)
    expect(alert.alert_items.count).to be > 0
    item_count = alert.alert_items.count
    
    expect { AlertMailer.alert_email(alert).deliver_now }.to change { alert.alert_items.count }.by -item_count
  end

end
