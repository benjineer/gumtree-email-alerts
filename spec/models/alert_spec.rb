require 'rails_helper'

RSpec.describe Alert, :type => :model do
  
  it 'allows only a past date for last run date' do
  	alert = create(:alert)
  	alert.update(last_run: DateTime.now - 1)
  	expect(alert.errors.count).to eq(0)

  	alert = create(:alert)
  	alert.update(last_run: DateTime.now + 1)
  	expect(alert.errors.count).not_to eq(0)
  end

  it 'allows a last run date to be updated only with a value greater than the previous value' do
  	alert = create(:alert)
  	alert.update(last_run: DateTime.now)
  	expect(alert.errors.count).to eq(0)

  	alert.update(last_run: DateTime.now)
  	expect(alert.errors.count).to eq(0)

  	alert.update(last_run: DateTime.now - 1)
  	expect(alert.errors.count).not_to eq(0)
	end

  it 'cleans the keyword, returning only aplhanumeric chars and \'+\'' do
  	alert = create(:alert)
  	alert.update(keyword: " p  $0! 30=t<>?,./~`@#%^&*()_-+2")
  	expect(alert.send(:url_keyword)).to eq('p+0+30t2')
  end

end
