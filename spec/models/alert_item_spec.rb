require 'rails_helper'

RSpec.describe AlertItem, :type => :model do
  
	it 'only saves if ref and alert combination are unique' do
		alert_item_1 = create(:alert_item)
		expect(alert_item_1.errors.count).to eq(0)

		alert_item_2 = build(:alert_item)
		expect(alert_item_2.errors.count).to eq(0)

		expect {
			alert_item_2.update({ ref: alert_item_1.ref, alert_id: alert_item_1.alert_id })
		}.to change { alert_item_2.errors.count }.by 1
	end

end
