require 'rails_helper'

RSpec.describe User, :type => :model do
  
	it 'limits alerts per user to 3' do
		user = create(:user)

		expect { 
			user.alerts << (create(:alert)) 
			user.save
		}.to change { user.errors.count }.by 0

		expect { 
			user.alerts << (create(:alert)) << (create(:alert)) 
			user.save
		}.to change { user.errors.count }.by 0

		expect { 
			user.alerts << (create(:alert)) 
			user.save
		}.to change { user.errors.count }.by 1
	end

	skip 'password requires a certain level of finesse'

end
