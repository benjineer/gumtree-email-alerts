require 'rails_helper'

RSpec.describe Category, :type => :model do
  
  it 'requires all fields to be set on creation' do
  	category = Category.create(name: nil, ref: nil, url_path: nil)
  	expect(category.errors.count).to eq(3)

  	category = Category.create(name: 'name', ref: 123, url_path: 'url-path')
  	expect(category.errors.count).to eq(0)
  end

end
