require 'rails_helper'

RSpec.describe "alerts/index", :type => :view do
  before(:each) do

    @user = create(:user)
    @category = create(:category)
    @location = create(:location)
    @last_run = DateTime.now

    @alerts = assign(:alerts, [
      Alert.create!(
        :name => "nomo",
        :keyword => "keeway",
        :user => @user,
        :category => @category,
        :min_price => 2.22,
        :max_price => 8.88,
        :offer_type => 'wanted',
        :price_type => "fixed",
        :last_run => @last_run,
        :location => @location,
        :radius => Location::RADIUS_KMS.first
      ),
      Alert.create!(
        :name => "nomo",
        :keyword => "keeway",
        :user => @user,
        :category => @category,
        :min_price => 2.22,
        :max_price => 8.88,
        :offer_type => "wanted",
        :price_type => "fixed",
        :last_run => @last_run,
        :location => @location,
        :radius => Location::RADIUS_KMS.first
      )
    ])
  end

  it "renders a list of alerts" do
    render

    alert = @alerts[0]

    assert_select "tr>td", :text => alert.name, :count => 2
    assert_select "tr>td", :text => alert.keyword, :count => 2
    assert_select "tr>td", :text => @category.name, :count => 2
    assert_select "tr>td", :text => number_to_currency(alert.min_price), :count => 2
    assert_select "tr>td", :text => number_to_currency(alert.max_price), :count => 2
    assert_select "tr>td", :text => alert.offer_type_str, :count => 2
    assert_select "tr>td", :text => alert.price_type_str, :count => 2
    assert_select "tr>td", :text => alert.location_str, :count => 2

    skip 'with delete buttons'
  end

  skip 'renders alert count and limit'

  skip 'renders location'

  describe 'user has less than the max number of alerts' do
    skip 'renders new alert link'

    describe 'no alerts' do
      skip 'renders "none"'
    end
  end

  describe 'user has max number of alerts' do
    skip "doesn't render new alert link"
  end

  describe 'after delete' do
    skip 'renders flash'
  end

  describe 'after create' do
    skip 'renders flash'
  end

end
