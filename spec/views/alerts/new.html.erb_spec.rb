require 'rails_helper'

RSpec.describe "alerts/new", :type => :view do

  before(:each) do
    user = create(:user)
    @category = create(:category)

    assign(:alert, Alert.new(
      :name => "nomo",
      :keyword => "weeward",
      :user => user,
      :category => @category,
      :min_price => 2.22,
      :max_price => 8.88,
      :offer_type => "wanted",
      :price_type => "fixed"
    ))
  end

  it "renders new alert form" do
    render

    assert_select "form[action=?][method=?]", alerts_path, "post" do

      assert_select "input#alert_name[name=?]", "alert[name]"

      assert_select "input#alert_keyword[name=?]", "alert[keyword]"

      assert_select "select[name=?]", "alert[category_id]"

      assert_select "input#alert_min_price[name=?]", "alert[min_price]"

      assert_select "input#alert_max_price[name=?]", "alert[max_price]"

      assert_select "select#alert_offer_type[name=?]", "alert[offer_type]"

      assert_select "select#alert_price_type[name=?]", "alert[price_type]"
    end
  end

  skip 'renders save and cancel buttons'

  skip 'provides "all" option on all checkboxes'

  skip 'shows all categories'

  skip 'shows all locations'

  describe 'error saving' do
    skip 'renders error message'
    skip 'renders alert form again with data'
  end
  
end
