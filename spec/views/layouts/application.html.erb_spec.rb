require 'rails_helper'

RSpec.describe "layouts/application", :type => :view do

	skip 'renders title'
	skip 'renders copyright'

	describe 'logged in' do
  	skip 'renders username'
  	skip 'renders logout link'
  	skip 'renders manage account link'
  	skip "doesn't render login link"
  end

  describe 'logged out' do
	  skip 'renders login link'
	end

	describe 'has flash messages' do

		skip 'renders notice flash', "doesn't work with latest rspec" do
			flash[:notice] = 'notice_flash'
			assert_select 'div.alert-info', text: flash[:notice], count: 1
		end

		skip 'renders alert flash', "doesn't work with latest rspec" do
			flash[:alert] = 'alert_flash'
			assert_select 'div.alert-danger', text: flash[:alert], count: 1
		end
	end
  
end
