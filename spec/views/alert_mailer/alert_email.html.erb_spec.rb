require 'rails_helper'
require 'cgi'

RSpec.describe "alert_mailer/alert_email", type: :view do

  ALERT_LIMIT = 20

  it "renders attributes for #{ALERT_LIMIT} random alerts", slow: true do

    all_possible_alerts(ALERT_LIMIT).each {
      |alert|

      alert.alert_items.create([{
        url: "http://www.gumtree.com.au/s-ad/lismore-region/rideshare-travel-partners/offering-a-free-ride-to-byron-brisnane/1052553944",
        ref: "1052553944",
        title: "OFFERING A FREE RIDE TO BYRON / BRISNANE",
        description: "i am looking for some company to drive with me to byron bay / brisbane.
          LEAVING TOMORROW MORNING 10 AM FRIDAY 25TH JULY COMPANION DROPED OUT !!
          heading straight down prob take 2 OR 3 DAYS days
          CALL ME IF INTERESTED
          CHEERS
          FAYE
          ******3460",
        image_url: nil,
        price: nil,
        area: "Lismore Region",
        town: nil,
        date: "24/07/2014"
        },
        {
        url: "http://www.gumtree.com.au/s-figs/1234",
        ref: "1234",
        title: "!>< & %",
        description: "gduyegcuyberc",
        image_url: "http://www.gumfewfr3tree.coferwfm.au/s-figs/12e3fe3f34",
        price: '$20',
        area: nil,
        town: 'Lismore Towne',
        date: nil
      }])

      @alert = alert
      render

      alert_description = Regexp.escape(CGI.escapeHTML(@alert.description))

      expect(rendered).to match(/#{alert_description}/)

      alert.alert_items.each {
        |item|

        item_description = Regexp.escape(CGI.escapeHTML(item.description)) unless item.description.blank?
        item_price = Regexp.escape(item.price) unless item.price.blank?

        expect(rendered).to match(/href=['"]#{item.url}['"]/)
        expect(rendered).to match(/#{CGI.escapeHTML(item.title)}/)
        expect(rendered).to match(/#{item_description}/)
        expect(rendered).to match(/src=['"]#{item.image_url}['"]/) unless item.image_url.blank?
        expect(rendered).to match(/#{item_price}/) unless item.price.blank?
        expect(rendered).to match(/#{CGI.escapeHTML(item.area)}/) unless item.area.blank?
        expect(rendered).to match(/#{CGI.escapeHTML(item.town)}/) unless item.town.blank?
        expect(rendered).to match(/#{item.date}/)
      }
    }

  end

  skip 'renders unsubscribe link'

  skip 'renders delete alert link'

end
