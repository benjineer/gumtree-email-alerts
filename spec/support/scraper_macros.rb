module ScraperMacros

	class ScrapeResult
		
		attr_accessor :start, :finish, :alert

		def last_run_ok
			return alert.last_run != nil && alert.last_run > start && alert.last_run < finish
		end

	end


	HTML_DIR_URL = 'file://' + Rails.root.join('spec/support/source/').to_s

	def scrape(alert, verbose = false, base_url = Alert::BASE_URL)		
		result = ScrapeResult.new
		result.alert = alert
		result.start = DateTime.now

		Scraper::Gumtree::Alerts.scrape(result.alert, verbose, base_url)
		result.finish = DateTime.now
		result.alert.reload
		return result
	end

	def scrape_file(filename, alert = create(:test_alert), verbose = false)
		alert.url = '/' + filename
		return scrape(alert, verbose, HTML_DIR_URL)
	end

end