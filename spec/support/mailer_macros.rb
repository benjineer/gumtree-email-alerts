
module MailerMacros

=begin
	class MboxEmail
		attr_reader :to, :from, :subject, :date

		def initialize(string)
			to = /To: (.+)\n/.match(string)
			from = /From: (.+)\n/.match(string)
			subject = /Subject: (.+)\n/.match(string)
			date = /Date: (.+)\n/.match(string)

			@to = to[1] if to
			@from = from[1] if from
			@subject = subject[1] if subject
			@date = DateTime.parse(date[1]) if date
		end
	end

	def receive_email(alert)
		subject = AlertMailer.subject(alert)
		matching_email = nil

		read_emails.each {
			|email|
			if email.subject == subject
				matching_email = email
				break
			end
		}

		clean_mbox
		return matching_email
	end

	def read_emails
		emails = []
		File.open(Rails.configuration.inbox_options[:mbox_path]) {
			|mbox|
			raw_message = mbox.gets("\n\nFrom ")
			emails.push(MboxEmail.new(raw_message))
		}
		return emails
	end

	def clean_mbox
		File.new(Rails.configuration.inbox_options[:mbox_path], 'w')
	end
=end

end