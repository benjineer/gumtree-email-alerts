module ModelMacros

	def all_categories
		cat_number = 1
		categories = []

		begin
			loop do
				name = "cat#{cat_number}".to_sym
				category = create(name)

				category_exists = false
				categories.each {
					|existing_category|
					if category.ref == existing_category.ref
						category_exists = true
						break
					end
				}

				categories.push(category) unless category_exists
				cat_number += 1
			end
		rescue
		end

		return categories
	end

	def all_locations
		loc_number = 1
		locations = []

		begin
			loop do
				name = "loc#{loc_number}".to_sym
				location = create(name)

				location_exists = false
				locations.each {
					|existing_location|
					if location.ref == existing_location.ref
						location_exists = true
						break
					end
				}

				locations.push(location) unless location_exists
				loc_number += 1
			end
		rescue
		end

		return locations
	end

	def random_category
		random = Random.new
		name = "cat#{random.rand(1..Category::COUNT)}".to_sym
		return create(name)
	end

	def random_location
		random = Random.new
		name = "loc#{random.rand(1..Location::COUNT)}".to_sym
		return create(name)
	end
  
	def all_possible_alerts(limit = 100)
		alerts = []
		user = create(:user)

		default_alert = Alert.new({
			keyword: 'wigs',
			min_price: 2,
			max_price: 1000,
			offer_type: Alert::OFFER_TYPES.values.first,
			price_type: Alert::PRICE_TYPES.values.first,
			radius: Location::RADIUS_KMS.first
		})

		search_fields = [
			:keyword,
	    :category_id,
	    :location_id,
	    :min_price,
	    :max_price,
	    :offer_type,
	    :price_type
		]

		# create all field presence combinations
		binary_array = []
		search_fields.each { |i| binary_array << 0 << 1 }
		combos = binary_array.combination(search_fields.count).to_a.uniq
		combos = combos.shuffle.first(limit)

		category_index = search_fields.index(:category_id)
		location_index = search_fields.index(:location_id)

		combos.each {
			|combo|
			alert = Alert.new({ radius: default_alert.radius })

			# scalar fields
			search_fields.each_index {
				|i|

				if combo[i] == 1 && i != category_index && i != location_index
					alert[search_fields[i]] = default_alert[search_fields[i]]
				end
			}

			# category and location
			alert.category = random_category if combo[category_index] == 1
			alert.location = random_location if combo[location_index] == 1

			alerts.push(alert)
		}

		ActiveRecord::Base.transaction do
			alerts.each_with_index {
				|alert, i|
				alert.name = "alert#{i + 1}"
				alert.user_id = user.id
				alert.save!
			}
		end

		alerts.each {
			|alert|
			alert.reload
		}

		return alerts
	end

end